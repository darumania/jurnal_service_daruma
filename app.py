import time
import pprint
import ast
from postgresql import get_pending_jobs, update_job, get_all_jobs
from jurnal import Jurnal
import logging
from _constants import *

logging.basicConfig(level=logging.DEBUG)

while True:
    pending_jobs = get_pending_jobs()
    if pending_jobs:
        jurnal = Jurnal(warehouse_name="Sunter Warehouse")
        for pending in pending_jobs:
            if 'sales' in ast.literal_eval(pending['data_input'])['topic']:
                logging.info(f"job_id: {pending['job_id']}")
                sal = jurnal.create_sales(data=ast.literal_eval(pending['data_input']))
                logging.info(f'sales response: {sal}')
                update_job(job_id=pending['job_id'], response=str(sal))
            elif 'purchase' in ast.literal_eval(pending['data_input'])['topic']:
                logging.info(f"job_id: {pending['job_id']}")
                pur = jurnal.create_purchase(data=ast.literal_eval(str(pending['data_input'])))
                logging.info(f'purchase response: {pur}')
                update_job(job_id=pending['job_id'], response=str(pur))
    else:
        logging.info('no pending jobs')

    time.sleep(5)


