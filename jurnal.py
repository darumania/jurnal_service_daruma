from sales import sales
from purchase import purchase
import _constants

class Jurnal:
    API = _constants.API_KEY
    def __init__(self, warehouse_name, api_key=None):
        self.warehous_name = warehouse_name
        if api_key:
            self.API = api_key

    def create_sales(self, data):
        return sales(data, self.warehous_name)

    def create_purchase(self, data):
        return purchase(data, self.warehous_name)