from main import get_list_of_products
import csv
import pprint
from postgresql import insert_prod

# print("downloading products, please wait..")
# products = get_list_of_products()
#
# keys = products[0].keys()
# with open("products.csv", "w") as f:
#     dict_writer = csv.DictWriter(f, keys)
#     dict_writer.writeheader()
#     dict_writer.writerows(products)
# print("products downloaded..")

# prods = []
# with open("products.csv", "r") as file:
#     csv_reader = csv.reader(file)
#     for row in csv_reader:
#         prods.append(dict(
#             name=row[0],
#             jurnal_id=row[1],
#             custom_id=row[2]
#         ))
#
# total_prod = len(prods)
# inserted = 0
# print(f"total products: {total_prod}")
# for p in prods:
#     print(f"name; {p['name']}, jurnal_id: {p['jurnal_id']}")
#     try:
#         insert_prod(name=p['name'], jurnal_id=p['jurnal_id'])
#     except:
#         pass
#     inserted += 1
#     print(f"{round(inserted / total_prod * 100, 2)}% was inserted")


term = "COD"

def term_converter(term):
    if term.lower() == "cod":
        return "Cash on Delivery"
    elif term.lower() == "cbd":
        return "Custom"
    elif term.lower() == 'net 15':
        return "Net 15"
    elif term.lower() == "net 60":
        return "Net 60"
    elif term.lower() == "new 30":
        return "Net 30"
    else:
        return "Custom"

print(term_converter(term))