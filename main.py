import _constants
import requests
import json
import pprint
from _constants import BANK_NAMES
import re
from postgresql import get_products, insert_prod

headers = {
        'apikey': _constants.API_KEY,
    }



def term_converter(term):
    if term.lower() == "cod":
        return "Cash on Delivery"
    elif term.lower() == "cbd":
        return "Custom"
    elif term.lower() == 'net15':
        return "Net 15"
    elif term.lower() == "net60":
        return "Net 60"
    elif term.lower() == "net30":
        return "Net 30"
    else:
        return "Custom"


def get_bank_name(bank_input):
    # if bank_input == None:
    #     bank_input = "Saldo Tokopedia"
    for bank in BANK_NAMES:
        if "tokopedia" in str(bank_input).lower():
            return {
                        'bank_name': 'Saldo Tokopedia',
                        'bank_description': 'Penerimaan pembayaran dari Tokopedia',
                        'category': 'Kas & Bank'
                    }
        t = bank['bank_description']
        i = bank_input
        rek_t = ' '.join(re.findall('\d+', t))
        rek_i = ' '.join(re.findall('\d+', i))
        if rek_i == rek_t:
            return bank
        else:
            return None

def create_contact(display_name,
                   email,
                   address,
                   people_type,
                   associate_company=None,
                   billing_address=None,
                   phone=None,
                   first_name=None,
                   last_name=None,
                   is_customer=None,
                   is_vendor=None,
                   mobile=None,
                   ):


    data_create_contact = {
                              "person": {
                                "id": None,
                                "display_name": display_name,
                                "title": None,
                                "first_name": first_name,
                                "middle_name": None,
                                "last_name": last_name,
                                "mobile": mobile,
                                "identity_type": None,
                                "identity_number": None,
                                "email": email,
                                "other_detail": None,
                                "associate_company": associate_company,
                                "phone": phone,
                                "fax": None,
                                "tax_no": None,
                                "archive": None,
                                "billing_address": billing_address,
                                "billing_address_no": None,
                                "billing_address_rt": None,
                                "billing_address_rw": None,
                                "billing_address_post_code": None,
                                "billing_address_kelurahan": None,
                                "billing_address_kecamatan": None,
                                "billing_address_kabupaten": None,
                                "billing_address_provinsi": None,
                                "address": billing_address,
                                "bank_account_details": [
                                  {
                                    "bank_name": None,
                                    "bank_branch": "",
                                    "bank_account_holder_name": "",
                                    "bank_account_number": ""
                                  }
                                ],
                                "default_ar_account_id": None,
                                "default_ap_account_id": None,
                                "disable_max_credit_limit": True,
                                "disable_max_debit_limit": True,
                                "max_credit_limit": None,
                                "max_debit_limit": None,
                                "term_id": None,
                                "people_type": people_type,
                                "is_customer": is_customer,
                                "is_vendor": is_vendor,
                                "contact_group_names": [
                                      "work",
                                      "family"
                                  ]
                              }
                        }
    url = 'https://api.jurnal.id/core/api/v1/contacts'

    response = requests.post(url=url, json=data_create_contact, headers=headers)



    return response


def add_bank_deposit(deposit_amount,
                     transaction_date,
                     transaction_no,
                     person_name,
                     deposit_to_name):

    url = 'https://api.jurnal.id/core/api/v1/bank_deposits'

    data_deposit = {
        "bank_deposit": {
            "deposit_to_name": deposit_to_name,
            "person_name": person_name,
            "transaction_date": transaction_date,
            "transaction_no": transaction_no,
            "memo": "memo goes here",
            "custom_id": transaction_no,
            "transaction_account_lines_attributes": [
                {
                    "account_name": "Kas",
                    "description": "description of account lines",
                    "credit": deposit_amount,
                    # "line_tax_id": 11562,
                    "line_tax_name": "PPN"
                }
            ]
        }
    }

    response = requests.post(url=url, headers=headers, json=data_deposit)
    return response.json()


def create_purchase_order(transaction_date,
                          shipping_date,
                          shipping_price,
                          shipping_address,
                          ship_via,
                          tracking_no,
                          address,
                          term_name,
                          due_date,
                          bank,
                          total_transaction,
                          discount_unit,
                          person_name,
                          warehouse_name,
                          email,
                          transaction_no,
                          memo,
                          products,
                          custom_id=None,
                          message=None,
                          reference_no=None,
                          is_shipped=True):

    url = 'https://api.jurnal.id/core/api/v1/purchase_orders'

    try:
        bank_name = get_bank_name(bank)
    except:
        bank_name = None
    print(f"term before converted: {term_name}")

    term_converted = term_converter(term_name)

    print(f"term converted: {term_converted}")

    pr = []
    for product in products:
        if 'jne' in str(product['name']).lower():
            pass
        else:
            pr.append(dict(
                price=float(product['price']),
                quantity=float(product['quantity'])
            ))

    to = []
    for pcs in pr:
        total = pcs['price'] * pcs['quantity'] + (10 / 100 * (pcs['price'] * pcs['quantity']))
        to.append(dict(
            total=total
        ))

    total_transaction = float()
    for i in to:
        total_transaction += i['total']

    deposit = total_transaction

    transaction_lines_attributes = []
    for product in products:
        product_id = get_product_id(name=product['name'], custom_id=product['uid'])

        if not product_id:
            # print("create product %s" % product['name'])
            p = create_product(name=product['name'],
                               product_code=product['catalog_code'],
                               sell_price=product['price'],
                               buy_price=product['price'],
                               unit_name="Pcs",
                               custom_id=product['uid'])

            if p.get('error_full_messages', None) is not None:
                if p.get('name', None):
                    print("product already satisfied (create_purchase_order)")
                else:
                    print(p.get('error_full_messages', None))

        try:
            product_name = product_id['product_name'] if product_id is not None else \
                get_product_id(name=product['name'], custom_id=product['uid'])['product_name']
        except:
            product_name = None

        if product_name is not None:
            transaction_lines_attributes.append(
                dict(
                    quantity=product['quantity'],
                    rate=product['price'],
                    # discount=100 - (float(product['price']) / float(product['price_before_discount']) * 100),
                    product_name=product_name,
                    line_tax_name="ppn"
                )
            )

    pprint.pprint(transaction_lines_attributes)

    if str(term_name) == 'CBD' and float(deposit) > float(total_transaction):

        data_purchase_order = {
            "purchase_order": {
                "transaction_date": transaction_date,
                "transaction_lines_attributes": transaction_lines_attributes,
                "shipping_date": shipping_date,
                "shipping_price": shipping_price,
                "shipping_address": shipping_address,
                "is_shipped": is_shipped,
                "ship_via": ship_via,
                "reference_no": reference_no,
                "tracking_no": tracking_no,
                "address": address,
                "term_name": str(term_converted),
                # "due_date": due_date,
                "deposit": total_transaction,
                "discount_unit": discount_unit,
                "discount_type_name": "Percent",
                "person_name": person_name,
                # "warehouse_name": warehouse_name,
                "witholding_account_name": bank_name.get('bank_name') if bank_name is not None else None,
                "witholding_value": float(deposit) - float(total_transaction),
                "witholding_type": "value",
                "refund_from_name": bank_name.get('bank_name') if bank_name is not None else None,
                "email": email,
                "transaction_no": transaction_no,
                "message": message,
                "memo": memo,
                "custom_id": custom_id
            }
        }

    elif str(term_name) != 'CBD' and float(deposit) > float(total_transaction):
        print("str(term_name) != 'CBD' and float(deposit) > float(total_transaction)")
        data_purchase_order = {
            "purchase_order": {
                "transaction_date": transaction_date,
                "transaction_lines_attributes": transaction_lines_attributes,
                "shipping_date": shipping_date,
                "shipping_price": shipping_price,
                "shipping_address": shipping_address,
                "is_shipped": is_shipped,
                "ship_via": ship_via,
                "reference_no": reference_no,
                "tracking_no": tracking_no,
                "address": address,
                "term_name": str(term_converted),
                # "due_date": due_date,
                # "deposit": deposit,
                "discount_unit": discount_unit,
                "discount_type_name": "Percent",
                "person_name": person_name,
                # "warehouse_name": warehouse_name,
                "witholding_account_name": bank_name.get('bank_name') if bank_name is not None else None,
                "witholding_value": float(deposit) - float(total_transaction),
                "witholding_type": "value",
                "refund_from_name": bank_name.get('bank_name') if bank_name is not None else None,
                "email": email,
                "transaction_no": transaction_no,
                "message": message,
                "memo": memo,
                "custom_id": custom_id
            }
        }
    elif str(term_name) == 'CBD':
        print("str(term_name) == 'CBD'")
        data_purchase_order = {
            "purchase_order": {
                "transaction_date": transaction_date,
                "transaction_lines_attributes": transaction_lines_attributes,
                "shipping_date": shipping_date,
                "shipping_price": shipping_price,
                "shipping_address": shipping_address,
                "is_shipped": is_shipped,
                "ship_via": ship_via,
                "reference_no": reference_no,
                "tracking_no": tracking_no,
                "address": address,
                "term_name": term_converted,
                # "discount_unit": discount_unit,
                # "discount_type_name": "Percent",
                "deposit": deposit,
                "person_name": str(person_name).strip(),
                "due_date": due_date,
                # "warehouse_name": warehouse_name,
                "email": email,
                "transaction_no": transaction_no,
                "message": message,
                "memo": memo,
                "custom_id": custom_id
            }
        }
    # elif str(term_name) != 'CBD':
    #     print("str(term_name) != 'CBD'")
    #     data_purchase_order = {
    #         "purchase_order": {
    #             "transaction_date": transaction_date,
    #             "transaction_lines_attributes": transaction_lines_attributes,
    #             "shipping_date": shipping_date,
    #             "shipping_price": shipping_price,
    #             "shipping_address": shipping_address,
    #             "is_shipped": is_shipped,
    #             "ship_via": ship_via,
    #             "reference_no": reference_no,
    #             "tracking_no": tracking_no,
    #             "address": address,
    #             "term_name": term_converted,
    #             "discount_unit": discount_unit,
    #             "discount_type_name": "Percent",
    #             "person_name": person_name,
    #             "due_date": due_date,
    #             # "warehouse_name": warehouse_name,
    #             "email": email,
    #             "transaction_no": transaction_no,
    #             "message": message,
    #             "memo": memo,
    #             "custom_id": custom_id
    #         }
    #     }
    elif str(term_name) != 'CBD':
        print("str(term_name) != 'CBD'")
        data_purchase_order = {
            "purchase_order": {
                "transaction_date": transaction_date,
                "transaction_lines_attributes": transaction_lines_attributes,
                "shipping_date": shipping_date,
                # "shipping_price": shipping_price,
                "shipping_address": shipping_address if shipping_address is not None else address,
                "is_shipped": is_shipped,
                "ship_via": ship_via,
                "reference_no": reference_no,
                "tracking_no": tracking_no,
                "address": address,
                "term_name": term_converted,
                "discount_unit": discount_unit,
                "discount_type_name": "Percent",
                "person_name": person_name,
                "due_date": due_date,
                # "warehouse_name": warehouse_name,
                "email": email,
                "transaction_no": transaction_no,
                "message": message,
                "memo": memo,
                "custom_id": custom_id
            }
        }

        # "transaction_date": transaction_date,
        # "transaction_lines_attributes": transaction_lines_attributes,
        # "shipping_date": shipping_date,
        # # "shipping_price": shipping_price,
        # "shipping_address": shipping_address if shipping_address is not None else address,
        # "is_shipped": is_shipped,
        # "ship_via": ship_via,
        # "reference_no": reference_no,
        # "tracking_no": tracking_no,
        # "address": address,
        # "term_name": term_converted,
        # "discount_unit": discount_unit,
        # "discount_type_name": "Percent",
        # "person_name": person_name,
        # # "due_date": due_date,
        # # "warehouse_name": warehouse_name,
        # "email": email,
        # "transaction_no": transaction_no,
        # "message": message,
        # "memo": memo,
        # "custom_id": custom_id

    response = requests.post(url=url, headers=headers, json=data_purchase_order)
    print("request body:")
    pprint.pprint(json.loads(response.request.body))

    return response.json()

def create_sales_order(transaction_date,
                       shipping_date,
                       reference_no,
                       tracking_no,
                       address,
                       due_date,
                       person_name,
                       warehouse_name,
                       bank,
                       discount_unit,
                       transaction_no,
                       ship_via,
                       products,
                       shipping_price,
                       cashback=None,
                       is_shipped=True,
                       term_name=None,
                       shipping_address=None,
                       email=None,
                       custom_id=None,
                       memo=None,
                       message=None):

    url = 'https://api.jurnal.id/core/api/v1/sales_orders'

    try:
        bank_name = get_bank_name(bank)
    except:
        bank_name = None

    term_converted = term_converter(term_name).strip()

    print(f"term converted: {term_converted}")

    pr = []
    for product in products:
        pr.append(dict(
            price=float(product['price']),
            quantity=float(product['quantity'])
        ))

    to = []
    for pcs in pr:
        total = pcs['price'] * pcs['quantity'] + (10 / 100 * (pcs['price'] * pcs['quantity']))
        to.append(dict(
            total=total
        ))

    total_transaction = float()
    for i in to:
        total_transaction += i['total']

    deposit = total_transaction

    transaction_lines_attributes = []

    for product in products:
        product_id = get_product_id(name=product['name'], custom_id=product['uid'])
        if not product_id:
            # print("create product %s" % product['name'])
            p = create_product(name=product['name'],
                               product_code=product['catalog_code'],
                               sell_price=product['price'],
                               buy_price=product['price'],
                               unit_name="Pcs",
                               custom_id=product['uid'])
            if p.get('error_full_messages', None) is not None:
                if p.get('name', None):
                    print("product already satisfied (create_sales_order)")
        tax = "ppn"
        if 'jne' in str(product['name']).lower():
            tax = None

        try:
            product_name=product_id['product_name'] if product_id is not None else get_product_id(name=product['name'], custom_id=product['uid'])['product_name']
        except:
            product_name=None

        if product_name is not None:
            transaction_lines_attributes.append(
                dict(
                    quantity=product['quantity'],
                    rate=product['price'],
                    # discount=100 - (float(product['price']) / float(product['price_before_discount']) * 100),
                    product_name=product_name,
                    line_tax_name=tax
                )
            )
        else:
            pass


    if cashback is not None:
        if str(term_name) == 'CBD' and float(deposit) > float(total_transaction):
            print(" str(term_name) == 'CBD' and float(deposit) > float(total_transaction) with cashback")
            data_sales_order = {
                "sales_order": {
                    "transaction_date": transaction_date,
                    "transaction_lines_attributes": transaction_lines_attributes,
                    "shipping_date": shipping_date,
                    # "shipping_price": shipping_price,
                    "shipping_address": shipping_address,
                    "is_shipped": is_shipped,
                    "ship_via": ship_via,
                    "reference_no": reference_no,
                    "tracking_no": tracking_no,
                    "address": address,
                    "term_name": term_converted,
                    "due_date": due_date,
                    "deposit": total_transaction,
                    "discount_unit": discount_unit,
                    "discount_type_name": "Percent",
                    "person_name": person_name,
                    # "warehouse_name": warehouse_name,
                    "witholding_account_name": "Cashback",
                    "witholding_value": cashback,
                    "witholding_type": "value",
                    "refund_from_name": bank_name.get('bank_name') if bank_name is not None else None,
                    "email": email,
                    "transaction_no": transaction_no,
                    "message": message,
                    "memo": memo,
                    "custom_id": custom_id,
                }
            }

        elif str(term_name) != 'CBD' and float(deposit) > float(total_transaction):
            print("str(term_name) != 'CBD' and float(deposit) > float(total_transaction) with cashback")
            data_sales_order = {
                "sales_order": {
                    "transaction_date": transaction_date,
                    "transaction_lines_attributes": transaction_lines_attributes,
                    "shipping_date": shipping_date,
                    # "shipping_price": shipping_price,
                    "shipping_address": shipping_address,
                    "is_shipped": is_shipped,
                    "ship_via": ship_via,
                    "reference_no": reference_no,
                    "tracking_no": tracking_no,
                    "address": address,
                    "term_name": term_converted,
                    "due_date": due_date,
                    # "deposit": deposit,
                    "discount_unit": discount_unit,
                    "discount_type_name": "Percent",
                    "person_name": person_name,
                    # "warehouse_name": warehouse_name,
                    "witholding_account_name": "Cashback",
                    "witholding_value": cashback,
                    "witholding_type": "value",
                    "refund_from_name": bank_name.get('bank_name') if bank_name is not None else None,
                    "email": email,
                    "transaction_no": transaction_no,
                    "message": message,
                    "memo": memo,
                    "custom_id": custom_id
                }
            }
        elif str(term_name) == 'CBD':
            print("str(term_name) == 'CBD' with Cashback")
            data_sales_order = {
                "sales_order": {
                    "transaction_date": transaction_date,
                    "transaction_lines_attributes": transaction_lines_attributes,
                    "shipping_date": shipping_date,
                    # "shipping_price": shipping_price,
                    "shipping_address": shipping_address,
                    "is_shipped": is_shipped,
                    "ship_via": ship_via,
                    "reference_no": reference_no,
                    # "tracking_no": tracking_no,
                    "address": address,
                    "term_name": term_converted,
                    "discount_unit": discount_unit,
                    "discount_type_name": "Percent",
                    "deposit": deposit,
                    "person_name": person_name,
                    "due_date": due_date,
                    # "warehouse_name": warehouse_name,
                    "deposit_to_name": bank_name.get('bank_name') if bank_name is not None else None,
                    "witholding_account_name": "Cashback",
                    "witholding_value": cashback,
                    "witholding_type": "value",
                    "email": email,
                    "transaction_no": transaction_no,
                    "message": message,
                    "memo": memo,
                    "custom_id": custom_id
                }
            }
        elif str(term_name) != 'CBD':
            print("str(term_name) != 'CBD' with cashback")
            data_sales_order = {
                "sales_order": {
                    "transaction_date": transaction_date,
                    "transaction_lines_attributes": transaction_lines_attributes,
                    "shipping_date": shipping_date,
                    # "shipping_price": shipping_price,
                    "shipping_address": shipping_address if shipping_address is not None else address,
                    "is_shipped": is_shipped,
                    "ship_via": ship_via,
                    "reference_no": reference_no,
                    "tracking_no": tracking_no,
                    "address": address,
                    "term_name": term_converted,
                    "discount_unit": discount_unit,
                    "discount_type_name": "Percent",
                    "person_name": person_name,
                    # "due_date": due_date,
                    # "warehouse_name": warehouse_name,
                    "email": email,
                    "transaction_no": transaction_no,
                    "message": message,
                    "memo": memo,
                    "custom_id": custom_id
                }
            }
    else:
        if str(term_name) == 'CBD' and float(deposit) > float(total_transaction):
            print(" str(term_name) == 'CBD' and float(deposit) > float(total_transaction)")
            data_sales_order = {
                "sales_order": {
                    "transaction_date": transaction_date,
                    "transaction_lines_attributes": transaction_lines_attributes,
                    "shipping_date": shipping_date,
                    # "shipping_price": shipping_price,
                    "shipping_address": shipping_address,
                    "is_shipped": is_shipped,
                    "ship_via": ship_via,
                    "reference_no": reference_no,
                    "tracking_no": tracking_no,
                    "address": address,
                    "term_name": term_converted,
                    "due_date": due_date,
                    "deposit": total_transaction,
                    "discount_unit": discount_unit,
                    "discount_type_name": "Percent",
                    "person_name": person_name,
                    # "warehouse_name": warehouse_name,
                    "witholding_account_name": "Cashback",
                    "witholding_value": float(deposit) - float(total_transaction),
                    "witholding_type": "value",
                    "refund_from_name": bank_name.get('bank_name') if bank_name is not None else None,
                    "email": email,
                    "transaction_no": transaction_no,
                    "message": message,
                    "memo": memo,
                    "custom_id": custom_id,
                }
            }

        elif str(term_name) != 'CBD' and float(deposit) > float(total_transaction):
            print("str(term_name) != 'CBD' and float(deposit) > float(total_transaction)")
            data_sales_order = {
                "sales_order": {
                    "transaction_date": transaction_date,
                    "transaction_lines_attributes": transaction_lines_attributes,
                    "shipping_date": shipping_date,
                    # "shipping_price": shipping_price,
                    "shipping_address": shipping_address,
                    "is_shipped": is_shipped,
                    "ship_via": ship_via,
                    "reference_no": reference_no,
                    "tracking_no": tracking_no,
                    "address": address,
                    "term_name": term_converted,
                    # "due_date": due_date,
                    # "deposit": deposit,
                    "discount_unit": discount_unit,
                    "discount_type_name": "Percent",
                    "person_name": person_name,
                    # "warehouse_name": warehouse_name,
                    "witholding_account_name": bank_name.get('bank_name') if bank_name is not None else None,
                    "witholding_value": float(deposit) - float(total_transaction),
                    "witholding_type": "value",
                    "refund_from_name": bank_name.get('bank_name') if bank_name is not None else None,
                    "email": email,
                    "transaction_no": transaction_no,
                    "message": message,
                    "memo": memo,
                    "custom_id": custom_id
                }
            }
        elif str(term_name) == 'CBD':
            print("str(term_name) == 'CBD'")
            data_sales_order = {
                "sales_order": {
                    "transaction_date": transaction_date,
                    "transaction_lines_attributes": transaction_lines_attributes,
                    "shipping_date": shipping_date,
                    # "shipping_price": shipping_price,
                    "shipping_address": shipping_address,
                    "is_shipped": is_shipped,
                    "ship_via": ship_via,
                    "reference_no": reference_no,
                    "tracking_no": tracking_no,
                    "address": address,
                    "term_name": term_converted,
                    "discount_unit": discount_unit,
                    "discount_type_name": "Percent",
                    "deposit": deposit,
                    "person_name": person_name,
                    "due_date": due_date,
                    # "warehouse_name": warehouse_name,
                    "deposit_to_name": bank_name.get('bank_name') if bank_name is not None and _constants.PRODUCTION is not True else "Kas",
                    # "witholding_value": 10,
                    # "witholding_type": "percent",
                    "email": email,
                    "transaction_no": transaction_no,
                    "message": message,
                    "memo": memo,
                    "custom_id": custom_id
                }
            }
        elif str(term_name) != 'CBD':
            print("str(term_name) != 'CBD'")
            data_sales_order = {
                "sales_order": {
                    "transaction_date": transaction_date,
                    "transaction_lines_attributes": transaction_lines_attributes,
                    "shipping_date": shipping_date,
                    # "shipping_price": shipping_price,
                    "shipping_address": shipping_address if shipping_address is not None else address,
                    "is_shipped": is_shipped,
                    "ship_via": ship_via,
                    "reference_no": reference_no,
                    "tracking_no": tracking_no,
                    "address": address,
                    "term_name": term_converted,
                    "discount_unit": discount_unit,
                    "discount_type_name": "Percent",
                    "person_name": person_name,
                    # "due_date": due_date,
                    # "warehouse_name": warehouse_name,
                    "email": email,
                    "transaction_no": transaction_no,
                    "message": message,
                    "memo": memo,
                    "custom_id": custom_id
                }
            }

    response = requests.post(url=url, headers=headers, json=data_sales_order)
    return response.json()


def create_sales_order_payment(person_name,
                               transaction_date,
                               transaction_no,
                               bank,
                               payment_method_name,
                               memo=None):

    url = 'https://api.jurnal.id/core/api/v1/sales_order_payments'

    bank_name = get_bank_name(bank)

    data_sales_order_payment = {
        "sales_order_payment": {
            "person_name": person_name,
            "transaction_date": transaction_date,
            "transaction_no": transaction_no,
            "payment_method_name": payment_method_name,
            "deposit_to_name": bank_name.get('bank_name') if bank_name is not None else None,
            "memo": memo,
            "is_draft": False,
            "records_attributes": [
                {
                    "transaction_no": transaction_no,
                    "amount":  2112000
                }
            ]
        }
    }

    response = requests.post(url=url, headers=headers, json=data_sales_order_payment)
    return response.json()


def add_receive_payment_sales(amount,
                              custom_id,
                              transaction_date,
                              sales_id,
                              bank,
                              payment_method_name,
                              cashback=None,
                              memo=None):
    url = 'https://api.jurnal.id/core/api/v1/receive_payments'

    bank_name = get_bank_name(bank)

    if cashback is not None:
        amount = int(float(amount)) - int(float(cashback))

    receive_payment_data = {
        "receive_payment": {
            "transaction_date": transaction_date,
            "records_attributes": [
                {
                    "transaction_no": sales_id,
                    "amount": amount
                }
            ],
            "custom_id": custom_id,
            "payment_method_name": payment_method_name,
            "is_draft": False,
            "deposit_to_name": bank_name.get('bank_name') if bank_name is not None and _constants.PRODUCTION is True else "Kas",
            "memo": memo,
        }
    }

    pprint.pprint(receive_payment_data)

    response = requests.post(url=url, headers=headers, json=receive_payment_data)
    return response.json()


def update_receive_payment(
        pembulatan,
        transaction_date,
        payment_id):
    url = f'https://api.jurnal.id/core/api/v1/receive_payments/{payment_id}'

    receive_payment_data = {
        "receive_payment": {
            "transaction_date": transaction_date,
            "witholding_account_name": "Beban Selisih Pembulatan",
            "witholding_value": pembulatan,
            "witholding_type": "value",
        }
    }

    pprint.pprint(receive_payment_data)

    response = requests.patch(url=url, headers=headers, json=receive_payment_data)
    return response.json()

def add_receive_payment_purchase(person_name,
                                 transaction_no,
                                 transaction_date,
                                 purchase_id,
                                 bank,
                                 amount_receive_payment,
                                 payment_method_name="Cash",
                                 memo=None):

    url = 'https://api.jurnal.id/core/api/v1/purchase_payments'

    bank_name = get_bank_name(bank)

    receive_payment_data = {
        "purchase_payment": {
            "transaction_date": transaction_date,
            "transaction_no": transaction_no,
            "records_attributes": [
                {
                    "transaction_no": purchase_id,
                    "amount": amount_receive_payment
                }
            ],
            "person_name": person_name,
            "payment_method_name": payment_method_name,
            "memo": memo,
            "refund_from_name": bank_name.get('bank_name') if bank_name is not None else None,
            "custom_id": transaction_no,
            "is_draft": False
        }
    }

    response = requests.post(url=url, headers=headers, json=receive_payment_data)
    return response.json()


def create_warehouse(name,
                     code,
                     address,
                     description,
                     custom_id):
    url = 'https://api.jurnal.id/core/api/v1/warehouses/'

    warehouse_data = {
        "warehouse": {
            "name": name,
            "code": code,
            "address": address,
            "description": description,
            "custom_id": custom_id
        }
    }

    response = requests.post(url=url, headers=headers, json=warehouse_data)
    return response.json()


def create_purchase_delivery(person_name,
                             shipping_address,
                             transaction_date,
                             ship_via,
                             tracking_no,
                             transaction_no,
                             reference_no,
                             selected_po_id,
                             products,
                             shipping_price,
                             custom_id,
                             memo=None,
                             message=None,
                             tax_after_discount=True,
                             is_shipped=True):

    url = 'https://api.jurnal.id/core/api/v1/purchase_deliveries/'

    transaction_lines_attributes = []
    for product in products:
        if 'jne' in str(product['name']).lower():
            pass
        else:
            try:
                product_id = get_product_id(name=product['name'], custom_id=product['uid'])['product_id']
                print(f"product id: {product_id}")
            except:
                print("product id is None")
                return None
            # pprint.pprint(product_id)
            if not product_id:
                # print("create product %s" % product['name'])
                p = create_product(name=product['name'],
                                   product_code=product['catalog_code'],
                                   sell_price=product['price'],
                                   buy_price=product['price'],
                                   unit_name="Pcs",
                                   custom_id=product['uid'])
                if p.get('error_full_messages', None):
                    if p.get('name', None):
                        print("product already satisfied")
                        product_id = p.get('id')
            transaction_lines_attributes.append(dict(
                quantity=product['quantity'],
                product_id=product_id,
                description="",
                id="",
                # name=product_id['product_name']
            ))
    # # pprint.pprint(transaction_lines_attributes)
    data_purchase_delivery = {
        "purchase_delivery": {
            "tax_after_discount": tax_after_discount,
            "person_name": person_name,
            "email": "",
            "is_shipped": is_shipped,
            "shipping_address":shipping_address,
            "transaction_date": transaction_date,
            "ship_via": ship_via,
            "tracking_no": tracking_no,
            "transaction_no": transaction_no,
            "reference_no": reference_no,
            "selected_po_id": selected_po_id,
            "transaction_lines_attributes": transaction_lines_attributes,
            # "shipping_price": shipping_price,
            "message": message,
            "memo": memo,
            "custom_id": custom_id
        }
    }

    response = requests.post(url=url, headers=headers, json=data_purchase_delivery)

    print("request body: ")
    pprint.pprint(json.loads(response.request.body))
    pprint.pprint(response.json())

    err_response = response.json().get('error_full_messages', None)
    if err_response:
        if 'Product di transaction lines attributes untuk baris' in err_response[0]:
            pprint.pprint(response.json())
            return None

    return response.json()


# def convert_to_purchase_invoice(id):
#     url = 'https://api.jurnal.id/core/api/v1/purchase_deliveries/%s/convert_to_invoice' % id
#
#     response = requests.post(url=url, headers=headers)
#
#     return response.json()

def create_product(name,
                   product_code,
                   sell_price,
                   buy_price,
                   unit_name,
                   description=None,
                   custom_id=None,
                   inventory_asset_account_name="Persediaan Barang"):

  url = 'https://api.jurnal.id/core/api/v1/products'

  data_product = {
    "product": {
      "name": name,
      "sell_price_per_unit": sell_price,
      "custom_id": custom_id,
      "track_inventory": "true",
      "description": description,
      "unit_name": unit_name,
      "buy_price_per_unit": buy_price,
      "product_code": product_code,
      "is_bought": True,
      "buy_account_number": "5-50000",
      "buy_account_name": "Cost of Sales",
      "is_sold": True,
      "sell_account_number": "4-40000",
      "sell_account_name": "Service Revenue",
      "inventory_asset_account_name": inventory_asset_account_name,
      "taxable_sell": True,
    }
  }

  response = requests.post(url=url, headers=headers, json=data_product)

  # pprint.pprint(response.json())

  if response.json().get("product", None) is not None:
      insert_prod(name=name, jurnal_id=response.json()['product']['id'], custom_id=custom_id)
  elif response.json().get("id", None) is not None:
      insert_prod(name=name, jurnal_id=response.json()['id'], custom_id=custom_id)

  return response.json()

def create_sales_delivery(person_name,
                          shipping_address,
                          transaction_date,
                          ship_via,
                          tracking_no,
                          transaction_no,
                          selected_po_id,
                          shipping_price,
                          products,
                          custom_id,
                          memo=None,
                          message=None,
                          reference_no=None,
                          tax_after_discount=True,
                          email=None,
                          is_shipped=True):

    url = 'https://api.jurnal.id/core/api/v1/sales_deliveries/'


    transaction_lines_attributes = []



    for product in products:

        try:
            product_id = get_product_id(name=product['name'], custom_id=product['uid'])['product_id']
        except:
            print(product['name'])
            return None

        transaction_lines_attributes.append(dict(
            quantity=product['quantity'],
            product_id=product_id,
            description="",
            id=""
        ))


    data_sales_delivery = {
        "sales_delivery": {
            "tax_after_discount": tax_after_discount,
            "transaction_type_id": 21,
            "person_name": person_name,
            "email": email,
            "is_shipped": is_shipped,
            "shipping_address": shipping_address,
            "transaction_date": transaction_date,
            "ship_via": ship_via,
            "tracking_no": tracking_no,
            "transaction_no": transaction_no,
            "reference_no": reference_no,
            "tag_ids": None,
            "selected_po_id": selected_po_id,
            "transaction_lines_attributes": transaction_lines_attributes,
            # "shipping_price": shipping_price,
            "message": message,
            "memo": memo,
            "custom_id":custom_id,
        }
    }

    response = requests.post(url=url, headers=headers, json=data_sales_delivery)

    return response.json()


def convert_to_sales_invoice(id, invoice_date):

    url = 'https://api.jurnal.id/core/api/v1/sales_deliveries/%s/convert_to_invoice' % id

    body = {
        "purchase_delivery": {
            "transaction_date": invoice_date,
        }
    }

    response = requests.post(url=url, headers=headers, json=body)

    return response.json()


def convert_to_purchase_invoice(id, invoice_date):

    url = 'https://api.jurnal.id/core/api/v1/purchase_deliveries/%s/convert_to_invoice' % id

    body = {
        "purchase_delivery": {
            "transaction_date": invoice_date,
        }
    }

    response = requests.post(url=url, headers=headers, json=body)

    return response


def get_list_of_products(page=1):
    url = 'https://api.jurnal.id/core/api/v1/products'


    current_page = {'page': page}
    products = requests.get(url=url, headers=headers, params=current_page).json()
    if products.get('current_page', None) is None:
        pprint.pprint(products)
        return
    current_product_page = int(products['current_page'])
    total_product_page = int(products['total_pages'])
    result = []
    while current_product_page <= total_product_page:
        products = requests.get(url=url, headers=headers, params=current_page).json()
        current_product_page = int(products['current_page'])
        total_product_page = int(products['total_pages'])
        for product in products['products']:
            result.append(dict(
                name=product['name'],
                id=product['id'],
                custom_id=product['custom_id']
            ))
        current_page['page'] += 1

    return result


def get_contact_by_id(contact_id):
    url = f'https://api.jurnal.id/core/api/v1/contacts/{contact_id}'


    response = requests.get(url=url, headers=headers)

    return response.json()

def delete_po(id):

    url = f'https://api.jurnal.id/core/api/v1/purchase_orders/{id}'

    response = requests.delete(url=url, headers=headers)

    try:
        return response.json()
    except:
        return response.text

def create_sales_invoice(person_name,
                          term_name,
                          shipping_address,
                          transaction_date,
                          ship_via,
                          tracking_no,
                          shipping_date,
                          due_date,
                          transaction_no,
                          shipping_price,
                          products,
                          selected_po_id,
                          warehouse_name,
                          custom_id,
                          reference_no,
                          bank,
                          cashback=None,
                          memo=None,
                          email=None):

    url = 'https://api.jurnal.id/core/api/v1/sales_invoices'

    bank_name = get_bank_name(bank)
    print(f"selected_po_id: {selected_po_id}")

    pr = []
    for product in products:
        pr.append(dict(
            price=float(product['price']),
            quantity=float(product['quantity'])
        ))

    to = []
    for pcs in pr:
        total = pcs['price'] * pcs['quantity'] + (10 / 100 * (pcs['price'] * pcs['quantity']))
        to.append(dict(
            total=total
        ))

    total_transaction = float()
    for i in to:
        total_transaction += i['total']

    deposit = total_transaction

    transaction_lines_attributes = []
    for product in products:
        tax = "ppn"
        if 'jne' in str(product['name']).lower():
            tax = None
        transaction_lines_attributes.append(dict(
            quantity=product['quantity'],
            product_id=get_product_id(name=product['name'], custom_id=product['uid'])['product_id'],
            rate=product['price'],
            # product_name=product['name'],
            line_tax_name=tax
        ))

    if cashback is not None:
        if term_name.lower() == 'cbd':
            data_sales_invoice = {
                "sales_invoice": {
                    "transaction_date": transaction_date,
                    "transaction_lines_attributes": transaction_lines_attributes,
                    "shipping_date": shipping_date,
                    "shipping_price": shipping_price,
                    "shipping_address": shipping_address,
                    "is_shipped": True,
                    "ship_via": ship_via,
                    "tracking_no": tracking_no,
                    "address": shipping_address,
                    "term_name": term_name,
                    "due_date": due_date,
                    "witholding_account_name": "Kas" if _constants.PRODUCTION is False else "Cashback",
                    "witholding_value": cashback,
                    "witholding_type": "value",
                    # "deposit_to_name": bank_name.get('bank_name') if bank_name is not None and _constants.PRODUCTION is not False else "Kas",
                    "deposit_to_name": "Kas",
                    "deposit": deposit,
                    "person_name": person_name,
                    "warehouse_name": warehouse_name,
                    "email": email,
                    "memo": memo,
                    "selected_po_id": selected_po_id,
                    "transaction_no": transaction_no,
                    "custom_id": custom_id,
                    "reference_no": reference_no
                }
            }
        else:
            data_sales_invoice = {
                "sales_invoice": {
                    "transaction_date": transaction_date,
                    "transaction_lines_attributes": transaction_lines_attributes,
                    "shipping_date": shipping_date,
                    "shipping_price": shipping_price,
                    "shipping_address": shipping_address,
                    "is_shipped": True,
                    "ship_via": ship_via,
                    "tracking_no": tracking_no,
                    "address": shipping_address,
                    "term_name": term_name,
                    "due_date": due_date,
                    "witholding_account_name": "Cashback" if _constants.PRODUCTION is True else "Kas",
                    "witholding_value": cashback,
                    "witholding_type": "value",
                    "selected_po_id": selected_po_id,
                    "person_name": person_name,
                    "warehouse_name": warehouse_name,
                    "email": email,
                    "transaction_no": transaction_no,
                    "custom_id": custom_id,
                    "reference_no": reference_no
                }
            }

    else:
        if term_name.lower() == 'cbd':
            data_sales_invoice = {
                "sales_invoice": {
                    "transaction_date": transaction_date,
                    "transaction_lines_attributes": transaction_lines_attributes,
                    "shipping_date": shipping_date,
                    "shipping_price": shipping_price,
                    "shipping_address": shipping_address,
                    "is_shipped": True,
                    "ship_via": ship_via,
                    "selected_po_id": selected_po_id,
                    "tracking_no": tracking_no,
                    "address": shipping_address,
                    "term_name": term_name,
                    "total_transaction": total_transaction,
                    "due_date": due_date,
                    "deposit_to_name": bank_name.get('bank_name') if bank_name is not None and _constants.PRODUCTION is not False else "Kas",
                    "deposit": deposit,
                    "person_name": person_name,
                    "warehouse_name": warehouse_name,
                    "email": email,
                    "memo": memo,
                    "transaction_no": transaction_no,
                    "custom_id": custom_id,
                    "reference_no": reference_no
                }
            }
        else:
            data_sales_invoice = {
                "sales_invoice": {
                    "transaction_date": transaction_date,
                    "transaction_lines_attributes": transaction_lines_attributes,
                    "shipping_date": shipping_date,
                    "shipping_price": shipping_price,
                    "shipping_address": shipping_address,
                    "is_shipped": True,
                    "ship_via": ship_via,
                    "tracking_no": tracking_no,
                    "address": shipping_address,
                    "term_name": term_name,
                    "due_date": due_date,
                    "person_name": person_name,
                    "total_transaction": total_transaction,
                    "warehouse_name": warehouse_name,
                    "email": email,
                    "transaction_no": transaction_no,
                    "custom_id": custom_id,
                    "reference_no": reference_no,
                    "selected_po_id":selected_po_id
                }
            }

    response = requests.post(url=url, headers=headers, json=data_sales_invoice)

    return response.json()


def update_sales_invoice(person_name,
                          term_name,
                          shipping_address,
                          transaction_date,
                          ship_via,
                          tracking_no,
                          shipping_date,
                          due_date,
                          transaction_no,
                          shipping_price,
                          products,
                          selected_po_id,
                          warehouse_name,
                          custom_id,
                          reference_no,
                          bank,
                          sales_invoice_id,
                          cashback=None,
                          memo=None,
                          email=None):

    url = f'https://api.jurnal.id/core/api/v1/sales_invoices/{sales_invoice_id}'

    bank_name = get_bank_name(bank)
    print(f"sales_invoice_id: {sales_invoice_id}")

    transaction_lines_attributes_pembulatan = []

    for product in products:
        tax = "ppn"
        if 'jne' in str(product['name']).lower():
            tax = None
        transaction_lines_attributes_pembulatan.append(
            dict(
                quantity=product['quantity'],
                rate=product['price'],
                line_tax_name=tax
            )
        )

    sub_total = 0
    for price in transaction_lines_attributes_pembulatan:
        sub_total += float(price['rate'])

    sub_total = sub_total + (0.1 * sub_total)
    pembulatan = None
    if round(float(sub_total) - int(sub_total), 2) != 0.0:
        pembulatan = round(float(sub_total) - int(sub_total), 2)


    pr = []
    for product in products:
        pr.append(dict(
            price=float(product['price']),
            quantity=float(product['quantity'])
        ))

    to = []
    for pcs in pr:
        total = pcs['price'] * pcs['quantity'] + (10 / 100 * (pcs['price'] * pcs['quantity']))
        to.append(dict(
            total=total
        ))

    total_transaction = float()
    for i in to:
        total_transaction += i['total']

    deposit = total_transaction

    transaction_lines_attributes = []
    for product in products:
        try:
            prod_id = get_product_id(name=product['name'], custom_id=product['uid'])['product_id']
        except:
            prod_id = None

        if 'jne' in str(product['name']):
            transaction_lines_attributes.append(dict(
                quantity=product['quantity'],
                product_id=prod_id,
                rate=product['price'],
                line_tax_name=None
            ))
        else:
            transaction_lines_attributes.append(dict(
                quantity=product['quantity'],
                product_id=prod_id,
                rate=product['price'],
                line_tax_name="ppn"
            ))


    if cashback is not None and pembulatan is not None:
        if term_name.lower() == 'cbd':
            data_sales_invoice = {
                "sales_invoice": {
                    "transaction_date": transaction_date,
                    "transaction_lines_attributes": transaction_lines_attributes,
                    "shipping_date": shipping_date,
                    "shipping_price": shipping_price,
                    "shipping_address": shipping_address,
                    "is_shipped": True,
                    "ship_via": ship_via,
                    "tracking_no": tracking_no,
                    "address": shipping_address,
                    "term_name": term_name,
                    "due_date": due_date,
                    "witholding_account_name": "Kas" if _constants.PRODUCTION is False else "Cashback",
                    "witholding_value": cashback,
                    "witholding_type": "value",
                    # "deposit_to_name": bank_name.get('bank_name') if bank_name is not None and _constants.PRODUCTION is not False else "Kas",
                    "deposit_to_name": "Kas",
                    "deposit": deposit,
                    "person_name": person_name,
                    "warehouse_name": warehouse_name,
                    "email": email,
                    "memo": memo,
                    "transaction_no": transaction_no,
                    "custom_id": custom_id,
                    "reference_no": reference_no
                }
            }
        else:
            data_sales_invoice = {
                "sales_invoice": {
                    "transaction_date": transaction_date,
                    "shipping_date": shipping_date,
                    "transaction_lines_attributes": transaction_lines_attributes,
                    "shipping_price": shipping_price,
                    "shipping_address": shipping_address,
                    "is_shipped": True,
                    "ship_via": ship_via,
                    "tracking_no": tracking_no,
                    "address": shipping_address,
                    "term_name": term_name,
                    "due_date": due_date,
                    "witholding_account_name": "Cashback" if _constants.PRODUCTION is True else "Kas",
                    "witholding_value": cashback,
                    "witholding_type": "Value",
                    "person_name": person_name,
                    "warehouse_name": warehouse_name,
                    "email": email,
                    "transaction_no": transaction_no,
                    "custom_id": custom_id,
                    "reference_no": reference_no
                }
            }

    else:
        if pembulatan is not None and term_name.lower != 'cbd':
            print(f"pembulatan: {pembulatan}")
            data_sales_invoice = {
                "sales_invoice": {
                    "transaction_date": transaction_date,
                    "transaction_lines_attributes": transaction_lines_attributes,
                    "shipping_date": shipping_date,
                    "shipping_price": shipping_price,
                    "shipping_address": shipping_address,
                    "is_shipped": True,
                    "ship_via": ship_via,
                    "tracking_no": tracking_no,
                    "address": shipping_address,
                    "term_name": term_name,
                    "due_date": due_date,
                    "witholding_account_name": "Beban Selisih Pembulatan",
                    "witholding_value": pembulatan,
                    "witholding_type": "value",
                    "person_name": person_name,
                    "total_transaction": total_transaction,
                    "warehouse_name": warehouse_name,
                    "email": email,
                    "transaction_no": transaction_no,
                    "custom_id": custom_id,
                    "reference_no": reference_no,
                }
            }
        elif pembulatan is not None and term_name.lower == 'cbd':
            print(f"pembulatan: {pembulatan}")
            data_sales_invoice = {
                "sales_invoice": {
                    "transaction_date": transaction_date,
                    # "transaction_lines_attributes": transaction_lines_attributes,
                    "shipping_date": shipping_date,
                    "shipping_price": shipping_price,
                    "shipping_address": shipping_address,
                    "is_shipped": True,
                    "ship_via": ship_via,
                    "selected_po_id": selected_po_id,
                    "tracking_no": tracking_no,
                    "address": shipping_address,
                    "term_name": term_name,
                    "total_transaction": total_transaction,
                    "due_date": due_date,
                    "deposit_to_name": bank_name.get('bank_name') if bank_name is not None and _constants.PRODUCTION is not False else "Kas",
                    "deposit": deposit,
                    "person_name": person_name,
                    "witholding_account_name": "Beban Selisih Pembulatan",
                    "witholding_value": pembulatan,
                    "witholding_type": "value",
                    "warehouse_name": warehouse_name,
                    "email": email,
                    "memo": memo,
                    "transaction_no": transaction_no,
                    "custom_id": custom_id,
                    "reference_no": reference_no
                }
            }
        elif term_name.lower() == 'cbd':
            data_sales_invoice = {
                "sales_invoice": {
                    "transaction_date": transaction_date,
                    # "transaction_lines_attributes": transaction_lines_attributes,
                    "shipping_date": shipping_date,
                    "shipping_price": shipping_price,
                    "shipping_address": shipping_address,
                    "is_shipped": True,
                    "ship_via": ship_via,
                    "selected_po_id": selected_po_id,
                    "tracking_no": tracking_no,
                    "address": shipping_address,
                    "term_name": term_name,
                    "total_transaction": total_transaction,
                    "due_date": due_date,
                    "deposit_to_name": bank_name.get('bank_name') if bank_name is not None and _constants.PRODUCTION is not False else "Kas",
                    "deposit": deposit,
                    "person_name": person_name,
                    "warehouse_name": warehouse_name,
                    "email": email,
                    "memo": memo,
                    "transaction_no": transaction_no,
                    "custom_id": custom_id,
                    "reference_no": reference_no
                }
            }
        else:
            data_sales_invoice = {
                "sales_invoice": {
                    "transaction_date": transaction_date,
                    "transaction_lines_attributes": transaction_lines_attributes,
                    "shipping_date": shipping_date,
                    "shipping_price": shipping_price,
                    "shipping_address": shipping_address,
                    "is_shipped": True,
                    "ship_via": ship_via,
                    "tracking_no": tracking_no,
                    "address": shipping_address,
                    "term_name": term_name,
                    "due_date": due_date,
                    "person_name": person_name,
                    "total_transaction": total_transaction,
                    "warehouse_name": warehouse_name,
                    "email": email,
                    "transaction_no": transaction_no,
                    "custom_id": custom_id,
                    "reference_no": reference_no,
                }
            }

    response = requests.patch(url=url, headers=headers, json=data_sales_invoice)

    try:
        return response.json()
    except:
        print(response.text)
        print(response.status_code)
        return response.text


def get_list_of_terms():
    url = 'https://api.jurnal.id/core/api/v1/terms'

    response = requests.get(url=url, headers=headers)

    return response.json()

def get_list_of_accounts():
    url = 'https://api.jurnal.id/core/api/v1/accounts'

    response = requests.get(url=url, headers=headers)

    return response.json()

def get_list_of_sales_order_payment(transaction_no):

    url = 'https://api.jurnal.id/core/api/v1/sales_orders/%s/sales_order_payments' % transaction_no

    response = requests.get(url=url, headers=headers)

    return response.json()

def get_list_of_purchase_orders(page=1):

    url = 'https://api.jurnal.id/core/api/v1/purchase_orders/'

    data = {
        'page': page
    }

    response = requests.get(url=url, headers=headers, params=data)

    return response


def get_purchase_order(transaction_no):

    url = 'https://api.jurnal.id/core/api/v1/purchase_orders/%s' % transaction_no


    response = requests.get(url=url, headers=headers)

    return response

def get_sales_order(transaction_no):

    url = 'https://api.jurnal.id/core/api/v1/sales_orders/%s' % transaction_no


    response = requests.get(url=url, headers=headers)

    return response

def get_purchase_delivery(transaction_no):

    url = 'https://api.jurnal.id/core/api/v1/purchase_deliveries/%s' % transaction_no


    response = requests.get(url=url, headers=headers)

    return response

def get_sales_delivery(transaction_no):

    url = 'https://api.jurnal.id/core/api/v1/sales_deliveries/%s' % transaction_no


    response = requests.get(url=url, headers=headers)

    return response

def get_purchase_invoice(transaction_no):

    url = 'https://api.jurnal.id/core/api/v1/purchase_invoices/%s' % transaction_no


    response = requests.get(url=url, headers=headers)

    return response

def get_sales_invoice(transaction_no):

    url = 'https://api.jurnal.id/core/api/v1/sales_invoices/%s' % transaction_no


    response = requests.get(url=url, headers=headers)

    return response


def get_transaction_by_order_no(transaction_no, data):
    for d in data:
        if str(d['order_no']) == transaction_no:
            return d
    return "data not found"

def get_list_of_payments_from_purchase(transaction_no):

    url = 'https://api.jurnal.id/core/api/v1/purchase_invoices/%s/purchase_payments' % transaction_no


    response = requests.get(url=url, headers=headers)

    return response


def get_list_of_purchase_deliveries(page=1):

    url = 'https://api.jurnal.id/core/api/v1/purchase_deliveries'

    data = {
        'page': page
    }

    response = requests.get(url=url, headers=headers, params=data)

    return response



def get_product_id_by_custom_id(custom_id):
    products = get_list_of_products()

    if products:
        for product in products:
            if product['name'] == 'Penjualan':
                pass
            else:
                try:
                    if custom_id.lower() in product['custom_id'].lower():
                        return dict(
                            product_id=product['id'],
                            product_name=product['name']
                        )
                except:
                    pass

    return None

def get_product_id(name, custom_id=None):

    products = get_products()

    for prod in products:
        if str(prod['name']).lower() == str(name).lower():
            return dict(
                product_name=prod['name'],
                product_id=prod['jurnal_id']
            )

    if custom_id:
        for custom in products:
            if str(custom_id).lower() == str(custom['custom_id']).lower():
                return dict(
                    product_name=custom['name'],
                    product_id=custom['jurnal_id']
                )

    return None

def read_sample_input(filename):
  with open(filename) as file:
    return json.loads(file.read())


def get_list_of_sales_order():

    url = 'https://api.jurnal.id/core/api/v1/sales_orders/'

    response = requests.get(url=url, headers=headers)

    return response


def get_sales_order_id_by_transaction_no(transaction_no):
    sales_orders = get_list_of_sales_order().json()['sales_orders']

    for sales in sales_orders:
        if sales['transaction_no'] == transaction_no:
            return sales['id']

    return None

def get_purchase_order_id_by_transaction_no(transaction_no):

    page = 1
    purchase_orders = get_list_of_purchase_orders(page=page).json()
    current_purchase_orders_page = int(purchase_orders['current_page'])
    total_purchase_orders_page = int(purchase_orders['total_pages'])
    while current_purchase_orders_page <= total_purchase_orders_page:
        purchase_orders = get_list_of_purchase_orders(page=page).json()
        current_purchase_orders_page = int(purchase_orders['current_page'])
        total_purchase_orders_page = int(purchase_orders['total_pages'])
        for p in purchase_orders['purchase_orders']:
            if str(p.get('transaction_no')).strip() == str(transaction_no).strip():
                return p.get('id')
        page += 1

    return None


def checking_purchase_orders(transaction_no):

    page = 1
    purchase_orders = get_list_of_purchase_orders(page=page).json()
    current_purchase_orders_page = int(purchase_orders['current_page'])
    total_purchase_orders_page = int(purchase_orders['total_pages'])
    while current_purchase_orders_page <= total_purchase_orders_page:
        purchase_orders = get_list_of_purchase_orders(page=page).json()
        current_purchase_orders_page = int(purchase_orders['current_page'])
        total_purchase_orders_page = int(purchase_orders['total_pages'])
        for p in purchase_orders['purchase_orders']:
            if str(p.get('transaction_no')).strip() == str(transaction_no).strip():
                return p
        page += 1
    return None

def get_list_of_contacts():
    url = 'https://api.jurnal.id/core/api/v1/contacts'

    data = {
        "contact_index": "5"
    }

    response = requests.get(url=url, headers=headers)

    return response.json()

if __name__ == "__main__":
    # bank_accounts = get_list_of_accounts()['accounts']
    # for bank in bank_accounts:
    #     if 'tokopedia' in str(bank['name']).lower():
    #         print(f"bank name: {bank['name']}\nbank description: {bank['description']}\ncategory: {bank['category']}\n\n")
    # get_list_of_products()
    # print(get_product_id(name="Stanley Combination Wrench - Kunci Ring Pas Set 11Pcs , STMT80942-8"))


    # JNEREG | Shipping JNE REG
    # products = get_products()
    # for product in products:
    #     if str(product['name']).lower() == "Stanley Combination Wrench - Kunci Ring Pas Set 11Pcs , STMT80942-8".lower():
    #         print(product['name'], product['id'])

    # pprint.pprint(delete_po(53414958))
    pprint.pprint(get_list_of_terms())
    # print(get_bank_name("Saldo tokopedia"))
    # pprint.pprint(create_product(name="Singkong",
    #                              product_code="123445",
    #                              sell_price=25000,
    #                              buy_price=25000,
    #                              unit_name="biji"))
    # pprint.pprint(create_warehouse(name="Sunter Warehouse",
    #                                code="SW",
    #                                address="Jalan Sunter Agung, Jakarta Utara",
    #                                description="Warehouse daruma",
    #                                custom_id="SW"))