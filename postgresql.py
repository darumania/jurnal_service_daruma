import psycopg2 as pg
from db_config import config
from _constants import PRODUCTION
from datetime import datetime, timedelta
import os
import ast
import pprint
import csv
import json

params = config()

if PRODUCTION:
    db = "daruma_jurnal_jurnal_jobs_live"
else:
    db = "daruma_jurnal_jurnal_jobs"

def get_pending_jobs():
    """ query data from the vendors table """
    conn = None
    result = list()
    query = f"""
    SELECT * FROM {db} WHERE status='pending'
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            job_id = row[0]
            data_input = row[1]
            status = row[2]
            response = row[3]
            created_at = row[4]
            result.append(dict(
                job_id=job_id,
                data_input=data_input,
                status=status,
                response=response,
                created_at=created_at
            ))

        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def get_all_jobs():
    """ query data from the vendors table """
    conn = None
    result = list()
    query = f"""
    SELECT * FROM {db}
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            job_id = row[0]
            data_input = row[1]
            status = row[2]
            response = row[3]
            created_at = row[4]
            result.append(dict(
                job_id=job_id,
                data_input=data_input,
                status=status,
                response=response,
                created_at=created_at
            ))

        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def delete_all_rows():
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    DELETE FROM daruma_jurnal_jurnal_job
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (db,))
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def insert_prod(name, jurnal_id, custom_id):
    """ insert vendor into vendors table """

    check_prod = check_product(name=name, custom_id=custom_id)
    if check_prod:
        print(f"{name} already exists")
        return

    sql = """
    INSERT INTO jurnal_products(product_name, jurnal_id, custom_id) VALUES(%s, %s, %s)
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (name, jurnal_id, custom_id))

        # commit the changes
        conn.commit()
        print("%s was inserted into jurnal_products table.." % (name))

        # close the cursor
        cur.close()
    # except (Exception, pg.DatabaseError) as e:
    #     print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()

def check_product(name, custom_id):

    conn = None

    query = f"""
        SELECT EXISTS(SELECT 1 FROM jurnal_products WHERE product_name=%s OR custom_id=%s)
        """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (name, custom_id))

        rows = cur.fetchone()

        return rows[0]
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def update_job(job_id, response):

    # sql = """
    # INSERT INTO request(job_id, url, http_status_code, content_type) VALUES(%s, %s, %s, %s)
    # """

    sql = f"""
    UPDATE {db} SET status='done', response=%s WHERE job_id=%s
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (response, job_id,))

        # commit the changes
        conn.commit()
        print("job %s was updated.." % (job_id))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()


def get_products():
    """ query data from the vendors table """
    conn = None
    result = list()
    query = f"""
    SELECT * FROM jurnal_products
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            job_id = row[0]
            data_input = row[1]
            status = row[2]
            custom_id = row[3]
            result.append(dict(
                id=job_id,
                name=data_input,
                jurnal_id=status,
                custom_id=custom_id
            ))

        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()


# print(check_product(name='Stanley Combination Wrench - Kunci Ring Pas Set 11Pcs , STMT80942-8', custom_id="Singkong"))
